import { gql } from "@apollo/client";
import GameFragment from "./game.fragment";

export const CreateGameMutation = gql`
  mutation CreateGame {
    createGame {
      ...GameFragment
    }
  }

  ${GameFragment}
`;
