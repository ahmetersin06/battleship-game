import { gql } from "@apollo/client";

const WarshipFragment = gql`
  fragment WarshipFragment on Warship {
    id
    type
    size
  }
`;
export default WarshipFragment;
