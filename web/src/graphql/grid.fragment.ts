import { gql } from "@apollo/client";
import WarshipFragment from "./warship.fragment";

const PlayerFragment = gql`
  fragment GridFragment on Grid {
    height
    width
    fleet {
      ...WarshipFragment
    }
    subMergedShips {
      ...WarshipFragment
    }
    rows {
      points {
        X
        Y
        status
        hasShip
      }
    }
  }

  ${WarshipFragment}
`;
export default PlayerFragment;
