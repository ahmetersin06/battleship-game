import { gql } from "@apollo/client";
import GridFragment from "./grid.fragment";

const PlayerFragment = gql`
  fragment PlayerFragment on Player {
    id
    name
    score
    totalShoot
    enemyGrid {
      ...GridFragment
    }
  }

  ${GridFragment}
`;
export default PlayerFragment;
