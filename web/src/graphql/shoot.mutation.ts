import { gql } from "@apollo/client";
import GameFragment from "./game.fragment";

export const ShootMutation = gql`
  mutation Shoot($input: ShootInput!) {
    shoot(input: $input) {
      ...GameFragment
    }
  }

  ${GameFragment}
`;
