import { gql } from "@apollo/client";
import PlayerFragment from "./player.fragment";

const GameFragment = gql`
  fragment GameFragment on Game {
    id
    gridHeight
    gridWidth
    status
    bot {
      ...PlayerFragment
    }
    player {
      ...PlayerFragment
    }
  }

  ${PlayerFragment}
`;
export default GameFragment;
