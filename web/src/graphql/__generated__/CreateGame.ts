/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { GameStatus, GridPointStatus } from "./../../globalTypes";

// ====================================================
// GraphQL mutation operation: CreateGame
// ====================================================

export interface CreateGame_createGame_bot_enemyGrid_fleet {
  __typename: "Warship";
  id: string;
  type: string;
  size: number;
}

export interface CreateGame_createGame_bot_enemyGrid_subMergedShips {
  __typename: "Warship";
  id: string;
  type: string;
  size: number;
}

export interface CreateGame_createGame_bot_enemyGrid_rows_points {
  __typename: "GridPoint";
  X: number;
  Y: number;
  status: GridPointStatus;
  hasShip: boolean;
}

export interface CreateGame_createGame_bot_enemyGrid_rows {
  __typename: "GridRow";
  points: CreateGame_createGame_bot_enemyGrid_rows_points[];
}

export interface CreateGame_createGame_bot_enemyGrid {
  __typename: "Grid";
  height: number;
  width: number;
  fleet: CreateGame_createGame_bot_enemyGrid_fleet[];
  subMergedShips: CreateGame_createGame_bot_enemyGrid_subMergedShips[];
  rows: CreateGame_createGame_bot_enemyGrid_rows[];
}

export interface CreateGame_createGame_bot {
  __typename: "Player";
  id: string;
  name: string;
  score: number;
  totalShoot: number;
  enemyGrid: CreateGame_createGame_bot_enemyGrid;
}

export interface CreateGame_createGame_player_enemyGrid_fleet {
  __typename: "Warship";
  id: string;
  type: string;
  size: number;
}

export interface CreateGame_createGame_player_enemyGrid_subMergedShips {
  __typename: "Warship";
  id: string;
  type: string;
  size: number;
}

export interface CreateGame_createGame_player_enemyGrid_rows_points {
  __typename: "GridPoint";
  X: number;
  Y: number;
  status: GridPointStatus;
  hasShip: boolean;
}

export interface CreateGame_createGame_player_enemyGrid_rows {
  __typename: "GridRow";
  points: CreateGame_createGame_player_enemyGrid_rows_points[];
}

export interface CreateGame_createGame_player_enemyGrid {
  __typename: "Grid";
  height: number;
  width: number;
  fleet: CreateGame_createGame_player_enemyGrid_fleet[];
  subMergedShips: CreateGame_createGame_player_enemyGrid_subMergedShips[];
  rows: CreateGame_createGame_player_enemyGrid_rows[];
}

export interface CreateGame_createGame_player {
  __typename: "Player";
  id: string;
  name: string;
  score: number;
  totalShoot: number;
  enemyGrid: CreateGame_createGame_player_enemyGrid;
}

export interface CreateGame_createGame {
  __typename: "Game";
  id: string;
  gridHeight: number;
  gridWidth: number;
  status: GameStatus;
  bot: CreateGame_createGame_bot;
  player: CreateGame_createGame_player;
}

export interface CreateGame {
  createGame: CreateGame_createGame;
}
