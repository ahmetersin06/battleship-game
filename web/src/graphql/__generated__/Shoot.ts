/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ShootInput, GameStatus, GridPointStatus } from "./../../globalTypes";

// ====================================================
// GraphQL mutation operation: Shoot
// ====================================================

export interface Shoot_shoot_bot_enemyGrid_fleet {
  __typename: "Warship";
  id: string;
  type: string;
  size: number;
}

export interface Shoot_shoot_bot_enemyGrid_subMergedShips {
  __typename: "Warship";
  id: string;
  type: string;
  size: number;
}

export interface Shoot_shoot_bot_enemyGrid_rows_points {
  __typename: "GridPoint";
  X: number;
  Y: number;
  status: GridPointStatus;
  hasShip: boolean;
}

export interface Shoot_shoot_bot_enemyGrid_rows {
  __typename: "GridRow";
  points: Shoot_shoot_bot_enemyGrid_rows_points[];
}

export interface Shoot_shoot_bot_enemyGrid {
  __typename: "Grid";
  height: number;
  width: number;
  fleet: Shoot_shoot_bot_enemyGrid_fleet[];
  subMergedShips: Shoot_shoot_bot_enemyGrid_subMergedShips[];
  rows: Shoot_shoot_bot_enemyGrid_rows[];
}

export interface Shoot_shoot_bot {
  __typename: "Player";
  id: string;
  name: string;
  score: number;
  totalShoot: number;
  enemyGrid: Shoot_shoot_bot_enemyGrid;
}

export interface Shoot_shoot_player_enemyGrid_fleet {
  __typename: "Warship";
  id: string;
  type: string;
  size: number;
}

export interface Shoot_shoot_player_enemyGrid_subMergedShips {
  __typename: "Warship";
  id: string;
  type: string;
  size: number;
}

export interface Shoot_shoot_player_enemyGrid_rows_points {
  __typename: "GridPoint";
  X: number;
  Y: number;
  status: GridPointStatus;
  hasShip: boolean;
}

export interface Shoot_shoot_player_enemyGrid_rows {
  __typename: "GridRow";
  points: Shoot_shoot_player_enemyGrid_rows_points[];
}

export interface Shoot_shoot_player_enemyGrid {
  __typename: "Grid";
  height: number;
  width: number;
  fleet: Shoot_shoot_player_enemyGrid_fleet[];
  subMergedShips: Shoot_shoot_player_enemyGrid_subMergedShips[];
  rows: Shoot_shoot_player_enemyGrid_rows[];
}

export interface Shoot_shoot_player {
  __typename: "Player";
  id: string;
  name: string;
  score: number;
  totalShoot: number;
  enemyGrid: Shoot_shoot_player_enemyGrid;
}

export interface Shoot_shoot {
  __typename: "Game";
  id: string;
  gridHeight: number;
  gridWidth: number;
  status: GameStatus;
  bot: Shoot_shoot_bot;
  player: Shoot_shoot_player;
}

export interface Shoot {
  shoot: Shoot_shoot;
}

export interface ShootVariables {
  input: ShootInput;
}
