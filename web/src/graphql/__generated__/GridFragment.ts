/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { GridPointStatus } from "./../../globalTypes";

// ====================================================
// GraphQL fragment: GridFragment
// ====================================================

export interface GridFragment_fleet {
  __typename: "Warship";
  id: string;
  type: string;
  size: number;
}

export interface GridFragment_subMergedShips {
  __typename: "Warship";
  id: string;
  type: string;
  size: number;
}

export interface GridFragment_rows_points {
  __typename: "GridPoint";
  X: number;
  Y: number;
  status: GridPointStatus;
  hasShip: boolean;
}

export interface GridFragment_rows {
  __typename: "GridRow";
  points: GridFragment_rows_points[];
}

export interface GridFragment {
  __typename: "Grid";
  height: number;
  width: number;
  fleet: GridFragment_fleet[];
  subMergedShips: GridFragment_subMergedShips[];
  rows: GridFragment_rows[];
}
