/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { GameStatus, GridPointStatus } from "./../../globalTypes";

// ====================================================
// GraphQL fragment: GameFragment
// ====================================================

export interface GameFragment_bot_enemyGrid_fleet {
  __typename: "Warship";
  id: string;
  type: string;
  size: number;
}

export interface GameFragment_bot_enemyGrid_subMergedShips {
  __typename: "Warship";
  id: string;
  type: string;
  size: number;
}

export interface GameFragment_bot_enemyGrid_rows_points {
  __typename: "GridPoint";
  X: number;
  Y: number;
  status: GridPointStatus;
  hasShip: boolean;
}

export interface GameFragment_bot_enemyGrid_rows {
  __typename: "GridRow";
  points: GameFragment_bot_enemyGrid_rows_points[];
}

export interface GameFragment_bot_enemyGrid {
  __typename: "Grid";
  height: number;
  width: number;
  fleet: GameFragment_bot_enemyGrid_fleet[];
  subMergedShips: GameFragment_bot_enemyGrid_subMergedShips[];
  rows: GameFragment_bot_enemyGrid_rows[];
}

export interface GameFragment_bot {
  __typename: "Player";
  id: string;
  name: string;
  score: number;
  totalShoot: number;
  enemyGrid: GameFragment_bot_enemyGrid;
}

export interface GameFragment_player_enemyGrid_fleet {
  __typename: "Warship";
  id: string;
  type: string;
  size: number;
}

export interface GameFragment_player_enemyGrid_subMergedShips {
  __typename: "Warship";
  id: string;
  type: string;
  size: number;
}

export interface GameFragment_player_enemyGrid_rows_points {
  __typename: "GridPoint";
  X: number;
  Y: number;
  status: GridPointStatus;
  hasShip: boolean;
}

export interface GameFragment_player_enemyGrid_rows {
  __typename: "GridRow";
  points: GameFragment_player_enemyGrid_rows_points[];
}

export interface GameFragment_player_enemyGrid {
  __typename: "Grid";
  height: number;
  width: number;
  fleet: GameFragment_player_enemyGrid_fleet[];
  subMergedShips: GameFragment_player_enemyGrid_subMergedShips[];
  rows: GameFragment_player_enemyGrid_rows[];
}

export interface GameFragment_player {
  __typename: "Player";
  id: string;
  name: string;
  score: number;
  totalShoot: number;
  enemyGrid: GameFragment_player_enemyGrid;
}

export interface GameFragment {
  __typename: "Game";
  id: string;
  gridHeight: number;
  gridWidth: number;
  status: GameStatus;
  bot: GameFragment_bot;
  player: GameFragment_player;
}
