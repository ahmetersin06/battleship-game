/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { GridPointStatus } from "./../../globalTypes";

// ====================================================
// GraphQL fragment: PlayerFragment
// ====================================================

export interface PlayerFragment_enemyGrid_fleet {
  __typename: "Warship";
  id: string;
  type: string;
  size: number;
}

export interface PlayerFragment_enemyGrid_subMergedShips {
  __typename: "Warship";
  id: string;
  type: string;
  size: number;
}

export interface PlayerFragment_enemyGrid_rows_points {
  __typename: "GridPoint";
  X: number;
  Y: number;
  status: GridPointStatus;
  hasShip: boolean;
}

export interface PlayerFragment_enemyGrid_rows {
  __typename: "GridRow";
  points: PlayerFragment_enemyGrid_rows_points[];
}

export interface PlayerFragment_enemyGrid {
  __typename: "Grid";
  height: number;
  width: number;
  fleet: PlayerFragment_enemyGrid_fleet[];
  subMergedShips: PlayerFragment_enemyGrid_subMergedShips[];
  rows: PlayerFragment_enemyGrid_rows[];
}

export interface PlayerFragment {
  __typename: "Player";
  id: string;
  name: string;
  score: number;
  totalShoot: number;
  enemyGrid: PlayerFragment_enemyGrid;
}
