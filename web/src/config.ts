const Config = {
  GraphqlUrl:
    process.env.REACT_APP_GRAPHQL_URL || "http://localhost:8080/graphql",
};

export default Config;
