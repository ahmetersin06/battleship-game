/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

export enum GameStatus {
  CONTINUE = "CONTINUE",
  ENDED = "ENDED",
}

export enum GridPointStatus {
  HIT = "HIT",
  MISS = "MISS",
  NO_SHOT = "NO_SHOT",
}

export interface GridPointInput {
  X: number;
  Y: number;
}

export interface ShootInput {
  gameId: string;
  point: GridPointInput;
}

//==============================================================
// END Enums and Input Objects
//==============================================================
