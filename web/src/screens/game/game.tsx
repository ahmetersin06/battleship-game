import { Dispatch, Fragment, SetStateAction, useState } from "react";
import Grid from "./grid";
import { GameFragment } from "../../graphql/__generated__/GameFragment";
import Shoot from "./shoot";
import { Tab, Tabs } from "@mui/material";
import Stat from "./stat";

enum Board {
  PLAYER = "player_board",
  BOT = "bot_board",
}

export default function Game(props: {
  data: GameFragment;
  setGame: Dispatch<SetStateAction<GameFragment | undefined>>;
}) {
  const [selectedTab, setSelectedTab] = useState(Board.PLAYER);

  return (
    <Fragment>
      <Shoot setGame={props.setGame} game={props.data} />
      <Tabs
        value={selectedTab}
        onChange={(_, value) => setSelectedTab(value)}
        aria-label="basic tabs example"
      >
        <Tab value={Board.PLAYER} label="Player Shooting Board" />
        <Tab value={Board.BOT} label="Bot Shooting Board" />
      </Tabs>
      {selectedTab === Board.PLAYER && (
        <Fragment>
          <Stat player={props.data.player} />
          <Grid data={props.data.player.enemyGrid} />
        </Fragment>
      )}
      {selectedTab === Board.BOT && (
        <Fragment>
          <Stat player={props.data.bot} />
          <Grid data={props.data.bot.enemyGrid} />
        </Fragment>
      )}
    </Fragment>
  );
}
