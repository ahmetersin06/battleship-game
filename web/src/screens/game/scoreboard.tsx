import { Avatar, Chip, Grid, Typography } from "@mui/material";
import { GameFragment } from "../../graphql/__generated__/GameFragment";
import Stat from "./stat";

export default function Scoreboard(props: { data: GameFragment }) {
  let player = props.data.player;
  let bot = props.data.bot;
  const winner = player.score === player.totalShoot ? player : bot;
  return (
    <Grid container>
      <Grid container direction="row" sx={{ margin: 1 }}>
        <Grid item sm={3} justifyContent="center" paddingY={1}>
          <Typography variant="h5" component="span">
            Winner:
          </Typography>
        </Grid>
        <Grid item sm={9}>
          <Chip
            color="success"
            variant={"outlined"}
            avatar={<Avatar>{winner.name.at(0)}</Avatar>}
            label={winner.name}
            sx={{ height: 50, minWidth: 150, width: "100%" }}
          />
        </Grid>
      </Grid>
      <Grid direction="row" sx={{ margin: 1 }}>
        <Typography variant="h5" component="h4">
          Player
        </Typography>
        <Stat player={player} />
      </Grid>
      <Grid direction="row" sx={{ margin: 1 }}>
        <Typography variant="h5" component="h4">
          Bot
        </Typography>
        <Stat player={bot} />
      </Grid>
    </Grid>
  );
}
