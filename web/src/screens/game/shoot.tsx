import { Dispatch, Fragment, SetStateAction, useEffect, useState } from "react";
import { Button, Grid, TextField } from "@mui/material";
import RocketLaunchIcon from "@mui/icons-material/RocketLaunch";
import { GameFragment } from "../../graphql/__generated__/GameFragment";
import { useMutation } from "@apollo/client";
import { ShootMutation } from "../../graphql/shoot.mutation";

function alphabetCoordinateToGridPoint(
  alphabetCoordinate: string,
  gridWidth: number,
  gridHeight: number
): {
  X: number;
  Y: number;
} {
  if (alphabetCoordinate.toString().length !== 2) {
    alert(
      "Invalid coordinate! Please enter as (row, col), e.g. A5 (Max Grid size 10x10)"
    );
  }

  const Y = Number(alphabetCoordinate.toLowerCase().charCodeAt(0) - 96) - 1;
  const X = Number(alphabetCoordinate.at(1));
  if (X >= gridWidth || Y >= gridHeight) {
    alert("Coordinate could not be bigger than the grid size!");
  }

  return { X, Y };
}

export default function Shoot(props: {
  game: GameFragment;
  setGame: Dispatch<SetStateAction<GameFragment | undefined>>;
}) {
  const [shoot, { data }] = useMutation(ShootMutation);
  const [coordinateValue, setCoordinateValue] = useState("");

  const shootHandler = () => {
    const { gridHeight, gridWidth } = props.game;
    const { X, Y } = alphabetCoordinateToGridPoint(
      coordinateValue,
      gridWidth,
      gridHeight
    );
    shoot({
      variables: {
        input: { gameId: props.game.id, point: { X, Y } },
      },
    });
    setCoordinateValue("");
  };

  useEffect(() => {
    if (data) {
      props.setGame(data.shoot);
    }
  });

  return (
    <Fragment>
      <Grid container direction="row">
        <Grid container item sm={8}>
          <TextField
            fullWidth
            id="standard-helperText"
            label="Shooting Coordinate"
            helperText="“Enter coordinates (row, col), e.g. A5“"
            variant="standard"
            value={coordinateValue}
            onChange={({ target: { value } }) => setCoordinateValue(value)}
            onKeyDown={({ key }) => {
              if (key === "Enter") {
                shootHandler();
              }
            }}
          />
        </Grid>
        <Grid container item sm={4} padding={2}>
          <Button
            variant="outlined"
            startIcon={<RocketLaunchIcon />}
            onClick={shootHandler}
          >
            Shoot
          </Button>
        </Grid>
      </Grid>
    </Fragment>
  );
}
