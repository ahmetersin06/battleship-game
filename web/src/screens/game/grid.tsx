import {
  Button,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
} from "@mui/material";
import { GridFragment } from "../../graphql/__generated__/GridFragment";
import { GridPointStatus } from "../../globalTypes";
import { Fragment, useState } from "react";
import CloseIcon from "@mui/icons-material/Close";
import RemoveIcon from "@mui/icons-material/Remove";
import CircleIcon from "@mui/icons-material/Circle";

const getCellValue = (gridPointStatus: GridPointStatus) => {
  switch (gridPointStatus) {
    case GridPointStatus.HIT:
      return <CloseIcon />;
    case GridPointStatus.MISS:
      return <RemoveIcon />;
    case GridPointStatus.NO_SHOT:
      return <CircleIcon />;
  }
};

const getDebugValue = (hasShip: boolean) => {
  return hasShip ? <CloseIcon /> : <CircleIcon visibility="hidden" />;
};

const getCharacterByIndex = (index: number) => {
  const characterCode = index + 96 + 1;

  return String.fromCharCode(characterCode).toUpperCase();
};

export default function Grid(props: { data: GridFragment }) {
  const [debug, setDebug] = useState(false);

  return (
    <Fragment>
      <Button onClick={() => setDebug(!debug)}>Debug</Button>
      <TableContainer>
        <Table>
          <TableBody>
            <TableRow key="table-head">
              <TableCell key="empty-cell" />
              {props.data.rows.map((_, index) => (
                <TableCell
                  key={`table-header-${index}`}
                  sx={{ fontSize: 30, fontWeight: "bold", padding: 0.5 }}
                >
                  {index}
                </TableCell>
              ))}
            </TableRow>
            {props.data.rows.map((row, rowIndex) => (
              <TableRow key={`row-${rowIndex}`}>
                <TableCell
                  key={`row-${rowIndex}-legend-${rowIndex}`}
                  sx={{ fontSize: 25, fontWeight: "bold", padding: 0.5 }}
                >
                  {getCharacterByIndex(rowIndex)}
                </TableCell>
                {row.points.map((point, columnIndex) => (
                  <TableCell
                    key={`row-${rowIndex}-column-${columnIndex}-`}
                    sx={{ fontSize: 25, fontWeight: "bold", padding: 0.5 }}
                  >
                    {debug
                      ? getDebugValue(point.hasShip)
                      : getCellValue(point.status)}
                  </TableCell>
                ))}
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Fragment>
  );
}
