import {
  Chip,
  Grid,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@mui/material";
import React, { Fragment } from "react";
import { PlayerFragment } from "../../graphql/__generated__/PlayerFragment";
import RocketLaunchIcon from "@mui/icons-material/RocketLaunch";
import HighlightOffIcon from "@mui/icons-material/HighlightOff";
import CallMissedOutgoingIcon from "@mui/icons-material/CallMissedOutgoing";

export default function Stat(props: { player: PlayerFragment }) {
  const paddingSx = { padding: 0.5 };
  return (
    <Fragment>
      <Grid>
        <Chip
          sx={{ margin: 1 }}
          icon={<RocketLaunchIcon />}
          label={`${props.player.totalShoot} Shoot`}
          variant="outlined"
        />
        <Chip
          sx={{ margin: 1 }}
          icon={<HighlightOffIcon />}
          label={`${props.player.score} Hit`}
          variant="outlined"
        />
        <Chip
          sx={{ margin: 1 }}
          icon={<CallMissedOutgoingIcon />}
          label={`${props.player.totalShoot - props.player.score} Miss`}
          variant="outlined"
        />
      </Grid>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Warship Type</TableCell>
              <TableCell>Size</TableCell>
              <TableCell>Submerged</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {props.player.enemyGrid.fleet.map((ship) => (
              <TableRow key={ship.id}>
                <TableCell sx={paddingSx}>{ship.type.toString()}</TableCell>
                <TableCell sx={paddingSx}>{ship.size}</TableCell>
                <TableCell sx={paddingSx}>
                  {props.player.enemyGrid.subMergedShips.some(
                    (item) => item.id === ship.id
                  )
                    ? "YES"
                    : "NO"}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Fragment>
  );
}
