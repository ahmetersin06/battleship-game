import { Button, Grid } from "@mui/material";
import Introduction from "./introduction";
import { Dispatch, SetStateAction, useEffect } from "react";
import { GameFragment } from "../../graphql/__generated__/GameFragment";
import { useMutation } from "@apollo/client";
import { CreateGameMutation } from "../../graphql/createGame.mutation";

export default function CreateGame(props: {
  setGame: Dispatch<SetStateAction<GameFragment | undefined>>;
}) {
  const [createGame, { data }] = useMutation(CreateGameMutation);

  useEffect(() => {
    if (data) {
      props.setGame(data.createGame);
    }
  });

  return (
    <Grid container alignItems="center" justifyContent="center" direction="row">
      <Grid item xs={12}>
        <Introduction />
      </Grid>
      <Grid item xs={10} container>
        <Button
          variant="contained"
          fullWidth={true}
          onClick={() => createGame()}
        >
          Start!
        </Button>
      </Grid>
    </Grid>
  );
}
