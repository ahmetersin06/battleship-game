import { Typography } from "@mui/material";

export default function Introduction() {
  return (
    <Typography align="center" sx={{ mt: 3, mb: 3 }} color="text.secondary">
      This a Battleship game implementation.
      <br />
      Press the button and play against to computer!
    </Typography>
  );
}
