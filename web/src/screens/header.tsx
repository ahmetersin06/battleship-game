import { Box, Typography } from "@mui/material";
import SailingIcon from "@mui/icons-material/Sailing";
import React from "react";

export default function Header() {
  return (
    <Box sx={{ my: 3 }}>
      <Typography align="center" variant="h3" component="h1" gutterBottom>
        <SailingIcon fontSize="large"></SailingIcon> Battleship Game
      </Typography>
    </Box>
  );
}
