import {
  Container,
  createTheme,
  CssBaseline,
  ThemeProvider,
} from "@mui/material";
import React, { useState } from "react";
import "./App.css";
import CreateGame from "./screens/createGame/createGame";
import Header from "./screens/header";
import { GameFragment } from "./graphql/__generated__/GameFragment";
import Game from "./screens/game/game";
import { GameStatus } from "./globalTypes";
import Scoreboard from "./screens/game/scoreboard";

const darkTheme = createTheme({
  palette: {
    mode: "dark",
  },
});

function App() {
  const [game, setGame] = useState<GameFragment>();

  return (
    <ThemeProvider theme={darkTheme}>
      <CssBaseline />
      <Header />
      <Container maxWidth="sm">
        {!game && <CreateGame setGame={setGame} />}
        {game && game.status === GameStatus.CONTINUE && (
          <Game data={game} setGame={setGame} />
        )}
        {game && game.status === GameStatus.ENDED && <Scoreboard data={game} />}
      </Container>
    </ThemeProvider>
  );
}

export default App;
