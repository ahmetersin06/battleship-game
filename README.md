# Battleship Game

| Node version |
|--------------|
| 16           |

### Running With Docker

```shell
### Docker compose v2
docker compose up
### Docker compose v1
docker-compose up
```
After containers up, 
* web server will be available on http://localhost:3000  
* graphql api will be available on http://localhost:8080/graphql

### Running Application

#### Api
```shell
cd api
cp .env.sample .env

yarn install
yarn start
# Graphql starts running on http://localhost:8080/graphql
```

#### Web
```shell
cd api
cp .env .env.local

yarn install
yarn start
# React app starts running on http://localhost:3000
```

## Links
* [Api documentation](api/README.md)
* [Web documentation](web/README.md)