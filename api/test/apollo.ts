import { ApolloServer } from 'apollo-server-express';
import typeDefs from '../src/transportation/graphql/typeDefs';
import resolvers from '../src/transportation/graphql/resolvers';
import GameManager from '../src/domain/game/gameManager';

export default function createApolloServer(
  gameManager: GameManager,
): ApolloServer {
  return new ApolloServer({
    typeDefs,
    resolvers,
    context: {
      gameManager,
    },
  });
}
