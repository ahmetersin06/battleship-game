import Player from '../../src/domain/player/player';
import Warship from '../../src/domain/ship/warship';
import Point from '../../src/domain/grid/point';

export default function shootShip(player: Player, ship: Warship) {
  const { start, end } = ship.getCoordinate();
  for (let xIndex = start.x; xIndex <= end.x; xIndex += 1) {
    for (let yIndex = start.y; yIndex <= end.y; yIndex += 1) {
      player.shoot(new Point(xIndex, yIndex));
    }
  }
}
