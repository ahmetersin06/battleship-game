import { ApolloServer } from 'apollo-server-express';
import GameManager from '../src/domain/game/gameManager';
import createApolloServer from './apollo';

export type TestSetup = {
  server: ApolloServer;
  gameManager: GameManager;
};

export async function setup(): Promise<TestSetup> {
  const gameManager = new GameManager();

  const server = createApolloServer(gameManager);
  return {
    server,
    gameManager,
  };
}
