import { gql } from 'apollo-server-express';
import { setup, TestSetup } from '../../graphql';
import { Game } from '../../../src/transportation/graphql/generated/graphql';

const CREATE_GAME = gql`
  mutation CreateGame {
    createGame {
      id
      status
      gridWidth
      gridHeight
      bot {
        id
        name
        score
        totalShoot
        enemyGrid {
          rows {
            points {
              X
              Y
            }
          }
          fleet {
            id
            size
            type
          }
          subMergedShips {
            id
            size
            type
          }
        }
      }
      player {
        id
        name
        score
        totalShoot
        enemyGrid {
          rows {
            points {
              X
              Y
            }
          }
          fleet {
            id
            size
            type
          }
          subMergedShips {
            id
            size
            type
          }
          height
          width
        }
      }
    }
  }
`;
describe('createGame mutation', () => {
  let testSetup: TestSetup;

  beforeAll(async () => {
    testSetup = await setup();
  });

  it('creates a game', async () => {
    const { server } = testSetup;

    const response = await server.executeOperation({
      query: CREATE_GAME,
    });

    const game: Game = response.data?.createGame;

    expect(game?.id).toBeDefined();
    expect(game?.gridHeight).toBe(10);
    expect(game?.gridWidth).toBe(10);

    expect(game?.player.id).toBeDefined();
    expect(game?.player.name).toBe(`Player-${game.player.id}`);
    expect(game?.player.score).toBe(0);
    expect(game?.player.totalShoot).toBe(0);
    expect(game?.player.enemyGrid.fleet).toHaveLength(3);
    expect(game?.player.enemyGrid.rows).toHaveLength(10);
    expect(game?.player.enemyGrid.subMergedShips).toHaveLength(0);

    expect(game?.bot.id).toBeDefined();
    expect(game?.bot.name).toBe(`Bot-${game.bot.id}`);
    expect(game?.bot.score).toBe(0);
    expect(game?.bot.totalShoot).toBe(0);
    expect(game?.bot.enemyGrid.fleet).toHaveLength(3);
    expect(game?.bot.enemyGrid.rows).toHaveLength(10);
    expect(game?.bot.enemyGrid.subMergedShips).toHaveLength(0);
  });
});
