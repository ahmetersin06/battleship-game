import { gql } from 'apollo-server-express';
import { GraphQLFormattedError } from 'graphql/error';
import { setup, TestSetup } from '../../graphql';
import { Game } from '../../../src/transportation/graphql/generated/graphql';

const SHOOT = gql`
  mutation Shoot($input: ShootInput!) {
    shoot(input: $input) {
      id
      status
      gridWidth
      gridHeight
      bot {
        id
        name
        score
        totalShoot
        enemyGrid {
          rows {
            points {
              X
              Y
            }
          }
          fleet {
            id
            size
            type
          }
          subMergedShips {
            id
            size
            type
          }
        }
      }
      player {
        id
        name
        score
        totalShoot
        enemyGrid {
          rows {
            points {
              X
              Y
            }
          }
          fleet {
            id
            size
            type
          }
          subMergedShips {
            id
            size
            type
          }
          height
          width
        }
      }
    }
  }
`;
describe('createGame mutation', () => {
  let testSetup: TestSetup;

  beforeAll(async () => {
    testSetup = await setup();
  });

  it('shoots the given point', async () => {
    const { server, gameManager } = testSetup;

    const game = gameManager.create();

    const response = await server.executeOperation({
      query: SHOOT,
      variables: {
        input: {
          gameId: game.getId(),
          point: {
            X: 5,
            Y: 1,
          },
        },
      },
    });

    const result: Game = response.data?.shoot;

    expect(result?.id).toBeDefined();
    expect(result?.gridHeight).toBe(10);
    expect(result?.gridWidth).toBe(10);

    expect(result?.player.id).toBeDefined();
    expect(result?.player.name).toBe(`Player-${result.player.id}`);
    expect(result?.player.totalShoot).toBe(1);
    expect(result?.player.enemyGrid.fleet).toHaveLength(3);
    expect(result?.player.enemyGrid.rows).toHaveLength(10);
    expect(result?.player.enemyGrid.subMergedShips).toHaveLength(0);

    expect(result?.bot.id).toBeDefined();
    expect(result?.bot.name).toBe(`Bot-${result.bot.id}`);
    expect(result?.bot.totalShoot).toBe(1);
    expect(result?.bot.enemyGrid.fleet).toHaveLength(3);
    expect(result?.bot.enemyGrid.rows).toHaveLength(10);
    expect(result?.bot.enemyGrid.subMergedShips).toHaveLength(0);
  });
  it('returns error if  the given point is out of grid range', async () => {
    const { server, gameManager } = testSetup;

    const game = gameManager.create();

    const response = await server.executeOperation({
      query: SHOOT,
      variables: {
        input: {
          gameId: game.getId(),
          point: {
            X: 15,
            Y: 1,
          },
        },
      },
    });

    const [error] = response.errors as [GraphQLFormattedError];
    const { extensions } = error;

    expect(extensions?.code).toBe('GRAPHQL_VALIDATION_FAILED');
  });
});
