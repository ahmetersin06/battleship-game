import { createLogger, format, transports } from 'winston';

const outputFormat = format.combine(format.colorize(), format.simple());

const logger = createLogger({
  transports: [
    new transports.Console({
      format: outputFormat,
      silent: process.env.NODE_ENV === 'test',
    }),
  ],
});

export default logger;
