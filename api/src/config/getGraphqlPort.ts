export default function getGraphqlPort(): string {
  const port = process.env.GRAPHQL_PORT;
  if (!port) {
    throw new Error('Please specify `GRAPHQL_PORT` in environment');
  }

  return port;
}
