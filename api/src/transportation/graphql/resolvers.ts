import { Resolvers } from './generated/graphql';

import Query from './query';
import Mutation from './mutation';
import Game from './resolver/type/game';
import Player from './resolver/type/player';
import GridPoint from './resolver/type/gridPoint';
import Warship from './resolver/type/warship';

const resolvers: Resolvers = {
  Query,
  Mutation,
  Game,
  Player,
  GridPoint,
  Warship,
};

export default resolvers;
