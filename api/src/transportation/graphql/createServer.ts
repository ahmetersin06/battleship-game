import { ApolloServerPluginLandingPageGraphQLPlayground } from 'apollo-server-core';
import { makeExecutableSchema } from '@graphql-tools/schema';
import { ApolloServer } from 'apollo-server-express';
import typeDefs from './typeDefs';
import resolvers from './resolvers';
import GameManager from '../../domain/game/gameManager';

export default function createServer(gameManager: GameManager): ApolloServer {
  return new ApolloServer({
    introspection: true,
    schema: makeExecutableSchema({ typeDefs, resolvers }),
    context: {
      gameManager,
    },
    plugins: [ApolloServerPluginLandingPageGraphQLPlayground()],
  });
}
