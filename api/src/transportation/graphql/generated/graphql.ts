import Context from '../context';

import { GraphQLResolveInfo } from 'graphql';
import GameDto from '../../../domain/game/game';
import PlayerDto from '../../../domain/player/player';
import { GridPoint as GridPointDto } from '../../../domain/player/exportEnemyGridAsTable';
import WarshipDto from '../../../domain/ship/warship';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
export type RequireFields<T, K extends keyof T> = Omit<T, K> & { [P in K]-?: NonNullable<T[P]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type Game = {
  __typename?: 'Game';
  bot: Player;
  gridHeight: Scalars['Int'];
  gridWidth: Scalars['Int'];
  id: Scalars['ID'];
  player: Player;
  status: GameStatus;
};

export enum GameStatus {
  Continue = 'CONTINUE',
  Ended = 'ENDED'
}

export type Grid = {
  __typename?: 'Grid';
  fleet: Array<Warship>;
  height: Scalars['Int'];
  rows: Array<GridRow>;
  subMergedShips: Array<Warship>;
  width: Scalars['Int'];
};

export type GridPoint = {
  __typename?: 'GridPoint';
  X: Scalars['Int'];
  Y: Scalars['Int'];
  hasShip: Scalars['Boolean'];
  status: GridPointStatus;
};

export type GridPointInput = {
  X: Scalars['Int'];
  Y: Scalars['Int'];
};

export enum GridPointStatus {
  Hit = 'HIT',
  Miss = 'MISS',
  NoShot = 'NO_SHOT'
}

export type GridRow = {
  __typename?: 'GridRow';
  points: Array<GridPoint>;
};

export type Mutation = {
  __typename?: 'Mutation';
  createGame: Game;
  shoot: Game;
};


export type MutationShootArgs = {
  input: ShootInput;
};

export type Player = {
  __typename?: 'Player';
  enemyGrid: Grid;
  id: Scalars['ID'];
  name: Scalars['String'];
  score: Scalars['Int'];
  totalShoot: Scalars['Int'];
};

export type Query = {
  __typename?: 'Query';
  game: Game;
};


export type QueryGameArgs = {
  gameId: Scalars['ID'];
};

export type ShootInput = {
  gameId: Scalars['ID'];
  point: GridPointInput;
};

export type Warship = {
  __typename?: 'Warship';
  id: Scalars['String'];
  size: Scalars['Int'];
  type: Scalars['String'];
};

export enum WarshipType {
  Battleship = 'BATTLESHIP',
  Destroyer = 'DESTROYER'
}

export type WithIndex<TObject> = TObject & Record<string, any>;
export type ResolversObject<TObject> = WithIndex<TObject>;

export type ResolverTypeWrapper<T> = Promise<T> | T;


export type ResolverWithResolve<TResult, TParent, TContext, TArgs> = {
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};
export type Resolver<TResult, TParent = {}, TContext = {}, TArgs = {}> = ResolverFn<TResult, TParent, TContext, TArgs> | ResolverWithResolve<TResult, TParent, TContext, TArgs>;

export type ResolverFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => Promise<TResult> | TResult;

export type SubscriptionSubscribeFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => AsyncIterable<TResult> | Promise<AsyncIterable<TResult>>;

export type SubscriptionResolveFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

export interface SubscriptionSubscriberObject<TResult, TKey extends string, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<{ [key in TKey]: TResult }, TParent, TContext, TArgs>;
  resolve?: SubscriptionResolveFn<TResult, { [key in TKey]: TResult }, TContext, TArgs>;
}

export interface SubscriptionResolverObject<TResult, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<any, TParent, TContext, TArgs>;
  resolve: SubscriptionResolveFn<TResult, any, TContext, TArgs>;
}

export type SubscriptionObject<TResult, TKey extends string, TParent, TContext, TArgs> =
  | SubscriptionSubscriberObject<TResult, TKey, TParent, TContext, TArgs>
  | SubscriptionResolverObject<TResult, TParent, TContext, TArgs>;

export type SubscriptionResolver<TResult, TKey extends string, TParent = {}, TContext = {}, TArgs = {}> =
  | ((...args: any[]) => SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>)
  | SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>;

export type TypeResolveFn<TTypes, TParent = {}, TContext = {}> = (
  parent: TParent,
  context: TContext,
  info: GraphQLResolveInfo
) => Maybe<TTypes> | Promise<Maybe<TTypes>>;

export type IsTypeOfResolverFn<T = {}, TContext = {}> = (obj: T, context: TContext, info: GraphQLResolveInfo) => boolean | Promise<boolean>;

export type NextResolverFn<T> = () => Promise<T>;

export type DirectiveResolverFn<TResult = {}, TParent = {}, TContext = {}, TArgs = {}> = (
  next: NextResolverFn<TResult>,
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

/** Mapping between all available schema types and the resolvers types */
export type ResolversTypes = ResolversObject<{
  Boolean: ResolverTypeWrapper<Scalars['Boolean']>;
  Game: ResolverTypeWrapper<GameDto>;
  GameStatus: GameStatus;
  Grid: ResolverTypeWrapper<Omit<Grid, 'fleet' | 'rows' | 'subMergedShips'> & { fleet: Array<ResolversTypes['Warship']>, rows: Array<ResolversTypes['GridRow']>, subMergedShips: Array<ResolversTypes['Warship']> }>;
  GridPoint: ResolverTypeWrapper<GridPointDto>;
  GridPointInput: GridPointInput;
  GridPointStatus: GridPointStatus;
  GridRow: ResolverTypeWrapper<Omit<GridRow, 'points'> & { points: Array<ResolversTypes['GridPoint']> }>;
  ID: ResolverTypeWrapper<Scalars['ID']>;
  Int: ResolverTypeWrapper<Scalars['Int']>;
  Mutation: ResolverTypeWrapper<{}>;
  Player: ResolverTypeWrapper<PlayerDto>;
  Query: ResolverTypeWrapper<{}>;
  ShootInput: ShootInput;
  String: ResolverTypeWrapper<Scalars['String']>;
  Warship: ResolverTypeWrapper<WarshipDto>;
  WarshipType: WarshipType;
}>;

/** Mapping between all available schema types and the resolvers parents */
export type ResolversParentTypes = ResolversObject<{
  Boolean: Scalars['Boolean'];
  Game: GameDto;
  Grid: Omit<Grid, 'fleet' | 'rows' | 'subMergedShips'> & { fleet: Array<ResolversParentTypes['Warship']>, rows: Array<ResolversParentTypes['GridRow']>, subMergedShips: Array<ResolversParentTypes['Warship']> };
  GridPoint: GridPointDto;
  GridPointInput: GridPointInput;
  GridRow: Omit<GridRow, 'points'> & { points: Array<ResolversParentTypes['GridPoint']> };
  ID: Scalars['ID'];
  Int: Scalars['Int'];
  Mutation: {};
  Player: PlayerDto;
  Query: {};
  ShootInput: ShootInput;
  String: Scalars['String'];
  Warship: WarshipDto;
}>;

export type GameResolvers<ContextType = Context, ParentType extends ResolversParentTypes['Game'] = ResolversParentTypes['Game']> = ResolversObject<{
  bot?: Resolver<ResolversTypes['Player'], ParentType, ContextType>;
  gridHeight?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  gridWidth?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  player?: Resolver<ResolversTypes['Player'], ParentType, ContextType>;
  status?: Resolver<ResolversTypes['GameStatus'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type GridResolvers<ContextType = Context, ParentType extends ResolversParentTypes['Grid'] = ResolversParentTypes['Grid']> = ResolversObject<{
  fleet?: Resolver<Array<ResolversTypes['Warship']>, ParentType, ContextType>;
  height?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  rows?: Resolver<Array<ResolversTypes['GridRow']>, ParentType, ContextType>;
  subMergedShips?: Resolver<Array<ResolversTypes['Warship']>, ParentType, ContextType>;
  width?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type GridPointResolvers<ContextType = Context, ParentType extends ResolversParentTypes['GridPoint'] = ResolversParentTypes['GridPoint']> = ResolversObject<{
  X?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  Y?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  hasShip?: Resolver<ResolversTypes['Boolean'], ParentType, ContextType>;
  status?: Resolver<ResolversTypes['GridPointStatus'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type GridRowResolvers<ContextType = Context, ParentType extends ResolversParentTypes['GridRow'] = ResolversParentTypes['GridRow']> = ResolversObject<{
  points?: Resolver<Array<ResolversTypes['GridPoint']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type MutationResolvers<ContextType = Context, ParentType extends ResolversParentTypes['Mutation'] = ResolversParentTypes['Mutation']> = ResolversObject<{
  createGame?: Resolver<ResolversTypes['Game'], ParentType, ContextType>;
  shoot?: Resolver<ResolversTypes['Game'], ParentType, ContextType, RequireFields<MutationShootArgs, 'input'>>;
}>;

export type PlayerResolvers<ContextType = Context, ParentType extends ResolversParentTypes['Player'] = ResolversParentTypes['Player']> = ResolversObject<{
  enemyGrid?: Resolver<ResolversTypes['Grid'], ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  score?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  totalShoot?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type QueryResolvers<ContextType = Context, ParentType extends ResolversParentTypes['Query'] = ResolversParentTypes['Query']> = ResolversObject<{
  game?: Resolver<ResolversTypes['Game'], ParentType, ContextType, RequireFields<QueryGameArgs, 'gameId'>>;
}>;

export type WarshipResolvers<ContextType = Context, ParentType extends ResolversParentTypes['Warship'] = ResolversParentTypes['Warship']> = ResolversObject<{
  id?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  size?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  type?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type Resolvers<ContextType = Context> = ResolversObject<{
  Game?: GameResolvers<ContextType>;
  Grid?: GridResolvers<ContextType>;
  GridPoint?: GridPointResolvers<ContextType>;
  GridRow?: GridRowResolvers<ContextType>;
  Mutation?: MutationResolvers<ContextType>;
  Player?: PlayerResolvers<ContextType>;
  Query?: QueryResolvers<ContextType>;
  Warship?: WarshipResolvers<ContextType>;
}>;

