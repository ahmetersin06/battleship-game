import { readFileSync } from 'fs';
import { gql } from 'apollo-server-express';

const schema = readFileSync(
  __dirname.concat('/../../../schema.graphql'),
  'utf-8',
);
const typeDefs = gql(schema);

export default typeDefs;
