import { Resolvers } from './generated/graphql';
import createGame from './resolver/mutation/createGame';
import shoot from './resolver/mutation/shoot';

const Mutation: Resolvers['Mutation'] = {
  createGame,
  shoot,
};

export default Mutation;
