import GameManager from '../../domain/game/gameManager';

export default interface Context {
  gameManager: GameManager;
}
