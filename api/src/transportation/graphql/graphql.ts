import { Server as HttpServer } from 'http';
import express from 'express';
import createServer from './createServer';
import logger from '../../logger';
import GameManager from '../../domain/game/gameManager';

export default async function setupGraphQLServer(
  port: string,
  gameManager: GameManager,
): Promise<HttpServer> {
  const server = createServer(gameManager);
  await server.start();

  const app = express();

  server.applyMiddleware({ app, path: '/graphql' });

  return app.listen(port, () => {
    logger.info(`🚀GraphQL API is running on port ${port}`);
  });
}
