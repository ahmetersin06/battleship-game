import { MutationResolvers } from '../../generated/graphql';

const createGame: MutationResolvers['createGame'] = async (
  _parent,
  _args,
  { gameManager },
) => {
  const game = await gameManager.create();

  return game;
};

export default createGame;
