import { ValidationError } from 'apollo-server-core';
import { MutationResolvers } from '../../generated/graphql';
import Point from '../../../../domain/grid/point';

const shoot: MutationResolvers['shoot'] = async (
  _parent,
  { input: { gameId, point } },
  { gameManager },
) => {
  try {
    const game = gameManager.getGame(gameId);
    game.shoot(new Point(point.X, point.Y));

    return game;
  } catch (e) {
    throw new ValidationError((e as Error).message);
  }
};

export default shoot;
