import {
  GameResolvers,
  GameStatus as GraphqlGameStatus,
} from '../../generated/graphql';
import { GameStatus } from '../../../../domain/game/game';

const Game: GameResolvers = {
  player(game) {
    return game.getNormalPlayer();
  },

  bot(game) {
    return game.getBotPlayer();
  },

  gridHeight(game) {
    return game.getGameOptions().gridHeight;
  },

  gridWidth(game) {
    return game.getGameOptions().gridWidth;
  },

  status(game) {
    if (game.getStatus() === GameStatus.CONTINUE) {
      return GraphqlGameStatus.Continue;
    }

    return GraphqlGameStatus.Ended;
  },
};

export default Game;
