import { PlayerResolvers } from '../../generated/graphql';
import exportEnemyGridAsTable from '../../../../domain/player/exportEnemyGridAsTable';

const Player: PlayerResolvers = {
  name(player) {
    return player.getName();
  },

  id(player) {
    return player.getId();
  },

  enemyGrid(player) {
    return exportEnemyGridAsTable(player);
  },

  score(player) {
    return player.getHits().length;
  },

  totalShoot(player) {
    return player.getShots().length;
  },
};

export default Player;
