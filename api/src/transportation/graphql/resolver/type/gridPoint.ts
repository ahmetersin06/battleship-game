import {
  GridPointResolvers,
  GridPointStatus as GraphqlGridPointStatus,
} from '../../generated/graphql';
import GridPointStatus from '../../../../domain/grid/gridPointStatus';

const GridPoint: GridPointResolvers = {
  status(gridPoint) {
    switch (gridPoint.status) {
      case GridPointStatus.HIT:
        return GraphqlGridPointStatus.Hit;
      case GridPointStatus.MISS:
        return GraphqlGridPointStatus.Miss;
      case GridPointStatus.NO_SHOOT:
        return GraphqlGridPointStatus.NoShot;
      default:
        throw new Error(`Unsupported grid point status ${gridPoint.status}`);
    }
  },
};

export default GridPoint;
