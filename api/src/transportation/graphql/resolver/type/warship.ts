import { WarshipResolvers, WarshipType } from '../../generated/graphql';
import Battleship from '../../../../domain/ship/battleship';
import Destroyer from '../../../../domain/ship/destroyer';

const Warship: WarshipResolvers = {
  id(warship) {
    return warship.getId();
  },

  size(warship) {
    return warship.getSize();
  },

  type(warship) {
    switch (true) {
      case warship instanceof Battleship:
        return WarshipType.Battleship;
      case warship instanceof Destroyer:
        return WarshipType.Destroyer;
      default:
        throw new Error('Unsupported warship!');
    }
  },
};

export default Warship;
