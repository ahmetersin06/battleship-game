import GameManager from './gameManager';
import GameNotFoundError from './error/gameNotFoundError';

function setup() {
  const gameManager = new GameManager();

  return {
    gameManager,
  };
}

describe('gameManager', () => {
  describe('create', () => {
    it('creates a game by default grid size', () => {
      const { gameManager } = setup();

      const game = gameManager.create();

      expect(game.getGameOptions().gridWidth).toBe(10);
      expect(game.getGameOptions().gridHeight).toBe(10);
    });
  });

  describe('getGame', () => {
    it('returns a game by its id', () => {
      const { gameManager } = setup();

      const game = gameManager.create();
      const result = gameManager.getGame(game.getId());

      expect(result).toMatchObject(game);
    });

    it('throws error if game not found', () => {
      const { gameManager } = setup();

      expect(() => gameManager.getGame('invalid-id')).toThrowError(
        GameNotFoundError,
      );
    });
  });
});
