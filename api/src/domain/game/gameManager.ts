import Game from './game';
import GameNotFoundError from './error/gameNotFoundError';

export default class GameManager {
  private readonly DEFAULT_GRID_WIDTH = 10;

  private readonly DEFAULT_GRID_HEIGHT = 10;

  private readonly games: Record<string, Game> = {};

  create(): Game {
    const game = new Game({
      gridHeight: this.DEFAULT_GRID_HEIGHT,
      gridWidth: this.DEFAULT_GRID_WIDTH,
    });

    this.games[game.getId()] = game;

    return game;
  }

  getGame(id: string): Game {
    const game = this.games[id];
    if (!game) {
      throw new GameNotFoundError(`Game#${id} not found.`);
    }

    return game;
  }
}
