import Game from './game';
import shootShip from '../../../test/helper/shootShip';
import Warship from '../ship/warship';
import Point from '../grid/point';
import GameEndedError from './error/gameEndedError';
import Destroyer from '../ship/destroyer';
import Battleship from '../ship/battleship';

function setup() {
  const game = new Game({ gridHeight: 10, gridWidth: 10 });

  return {
    game,
  };
}

describe('Game', () => {
  it('creates two destroyer and one battleship for normal player', () => {
    const { game } = setup();

    const normalPlayerFleet = game.getBotPlayer().getEnemyGrid().getFleet();
    const normalPlayerDestroyers = normalPlayerFleet.filter(
      (ship) => ship instanceof Destroyer,
    );
    const normalPlayerBattleships = normalPlayerFleet.filter(
      (ship) => ship instanceof Battleship,
    );
    expect(normalPlayerDestroyers).toHaveLength(2);
    expect(normalPlayerBattleships).toHaveLength(1);
  });

  it('creates two destroyer and one battleship for bot player', () => {
    const { game } = setup();

    const botPlayerFleet = game.getNormalPlayer().getEnemyGrid().getFleet();
    const botPlayerDestroyers = botPlayerFleet.filter(
      (ship) => ship instanceof Destroyer,
    );
    const botPlayerBattleships = botPlayerFleet.filter(
      (ship) => ship instanceof Battleship,
    );
    expect(botPlayerDestroyers).toHaveLength(2);
    expect(botPlayerBattleships).toHaveLength(1);
  });

  describe('getWinner', () => {
    it('returns the player if shots all the ships of the bot player', () => {
      const { game } = setup();

      const normalPlayer = game.getNormalPlayer();
      const fleet = normalPlayer.getEnemyGrid().getFleet();
      fleet.forEach((ship) => shootShip(normalPlayer, ship));

      expect(game.getWinner()).toMatchObject(normalPlayer);
    });

    it('returns the bot player if shots all the ships of the normal player', () => {
      const { game } = setup();

      const botPlayer = game.getBotPlayer();
      const fleet = botPlayer.getEnemyGrid().getFleet();
      fleet.forEach((ship) => shootShip(botPlayer, ship));

      expect(game.getWinner()).toMatchObject(botPlayer);
    });

    it('returns undefined if both two players have floating ships', () => {
      const { game } = setup();

      const botPlayer = game.getBotPlayer();
      const normalPlayer = game.getNormalPlayer();
      shootShip(botPlayer, botPlayer.getEnemyGrid().getFleet()[0] as Warship);
      shootShip(
        normalPlayer,
        normalPlayer.getEnemyGrid().getFleet()[0] as Warship,
      );

      expect(game.getWinner()).toBeUndefined();
    });
  });

  describe('shoot', () => {
    it('creates a shot by given point for player', () => {
      const { game } = setup();

      game.shoot(new Point(1, 1));

      expect(game.getNormalPlayer().getShots()).toHaveLength(1);
      expect(game.getNormalPlayer().getShots()[0]?.point.toString()).toBe(
        '1,1',
      );
    });
    it('creates a random shot for bot player ', () => {
      const { game } = setup();

      game.shoot(new Point(1, 1));

      expect(game.getBotPlayer().getShots()).toHaveLength(1);
      expect(game.getBotPlayer().getShots()[0]?.point).toBeInstanceOf(Point);
    });
    it('throws error if game status is ended', () => {
      const { game } = setup();

      const normalPlayer = game.getNormalPlayer();

      normalPlayer
        .getEnemyGrid()
        .getFleet()
        .forEach((ship) => {
          const { start, end } = ship.getCoordinate();
          for (let xIndex = start.x; xIndex <= end.x; xIndex += 1) {
            for (let yIndex = start.y; yIndex <= end.y; yIndex += 1) {
              game.shoot(new Point(xIndex, yIndex));
            }
          }
        });

      expect(() => game.shoot(new Point(1, 1))).toThrowError(GameEndedError);
    });
  });
});
