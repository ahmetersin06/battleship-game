import { v4 } from 'uuid';
import Player from '../player/player';
import Grid from '../grid/grid';
import NormalPlayer from '../player/normalPlayer';
import BotPlayer from '../player/botPlayer';
import Point from '../grid/point';
import GameEndedError from './error/gameEndedError';
import { WarshipType } from '../ship/warshipFactory';

export type GameOptions = {
  gridWidth: number;
  gridHeight: number;
};

export enum GameStatus {
  CONTINUE = 'continue',
  ENDED = 'ended',
}

export default class Game {
  private readonly id: string;

  private readonly normalPlayer: Player;

  private readonly botPlayer: Player;

  private readonly gameOptions: GameOptions;

  private status: GameStatus = GameStatus.CONTINUE;

  constructor(gameOptions: GameOptions) {
    this.gameOptions = gameOptions;

    const { gridWidth, gridHeight } = gameOptions;
    this.id = v4();

    const normalPlayerGrid = new Grid(gridWidth, gridHeight);
    normalPlayerGrid.addShipToRandomPoint(WarshipType.DESTROYER);
    normalPlayerGrid.addShipToRandomPoint(WarshipType.DESTROYER);
    normalPlayerGrid.addShipToRandomPoint(WarshipType.BATTLESHIP);

    const botPlayerGrid = new Grid(gridWidth, gridHeight);
    botPlayerGrid.addShipToRandomPoint(WarshipType.DESTROYER);
    botPlayerGrid.addShipToRandomPoint(WarshipType.DESTROYER);
    botPlayerGrid.addShipToRandomPoint(WarshipType.BATTLESHIP);

    this.normalPlayer = new NormalPlayer(botPlayerGrid);
    this.botPlayer = new BotPlayer(normalPlayerGrid);
  }

  getId(): string {
    return this.id;
  }

  getNormalPlayer(): Player {
    return this.normalPlayer;
  }

  getBotPlayer(): Player {
    return this.botPlayer;
  }

  getStatus(): GameStatus {
    return this.status;
  }

  getWinner(): Player | undefined {
    const threshold = this.normalPlayer.getEnemyGrid().getFleetSize();
    if (this.countPlayerHits(this.normalPlayer) >= threshold) {
      return this.normalPlayer;
    }
    if (this.countPlayerHits(this.botPlayer) >= threshold) {
      return this.botPlayer;
    }

    return undefined;
  }

  shoot(point: Point): void {
    if (this.status === GameStatus.ENDED) {
      throw new GameEndedError("Game ended. New shots can't be possible.");
    }

    this.normalPlayer.shoot(point);
    this.botPlayer.shootRandomly();

    this.updateStatusIfGameEnd();
  }

  getGameOptions(): GameOptions {
    return this.gameOptions;
  }

  private updateStatusIfGameEnd(): void {
    const winner = this.getWinner();
    if (!winner) {
      return;
    }

    this.status = GameStatus.ENDED;
  }

  private countPlayerHits(player: Player): number {
    return player.getHits().length;
  }
}
