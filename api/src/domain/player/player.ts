import Point from '../grid/point';
import Shot from './shot';
import Grid from '../grid/grid';

export default interface Player {
  getName(): string;
  shoot(point: Point): void;
  shootRandomly(): void;
  getShots(): Shot[];
  getHits(): Shot[];
  getId(): string;
  getEnemyGrid(): Grid;
}
