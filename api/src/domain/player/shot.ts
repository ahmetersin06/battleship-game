import Point from '../grid/point';

type Shot = {
  point: Point;
  warshipIdThatWasHit?: string;
};

export default Shot;
