import Player from './player';
import GridPointStatus from '../grid/gridPointStatus';
import Point from '../grid/point';
import Warship from '../ship/warship';

export type GridPoint = {
  X: number;
  Y: number;
  status: GridPointStatus;
  hasShip: boolean;
};
export type GridRow = { points: GridPoint[] };
export type GridTable = {
  rows: GridRow[];
  height: number;
  width: number;
  fleet: Warship[];
  subMergedShips: Warship[];
};

function findSubMergedShips(player: Player): Warship[] {
  const hitsByShip = player.getHits().reduce((data, shot) => {
    if (!shot.warshipIdThatWasHit) {
      return data;
    }

    return {
      ...data,
      [shot.warshipIdThatWasHit]: (data[shot.warshipIdThatWasHit] || 0) + 1,
    };
  }, {} as Record<string, number>);

  return player
    .getEnemyGrid()
    .getFleet()
    .filter((warship) => hitsByShip[warship.getId()] === warship.getSize());
}

export default function exportEnemyGridAsTable(player: Player): GridTable {
  const shots = player.getShots();
  const fleet = player.getEnemyGrid().getFleet();
  const yMax = player.getEnemyGrid().getHeight();
  const xMax = player.getEnemyGrid().getWidth();

  const gridTable: GridTable = {
    height: player.getEnemyGrid().getHeight(),
    width: player.getEnemyGrid().getWidth(),
    fleet: player.getEnemyGrid().getFleet(),
    subMergedShips: findSubMergedShips(player),
    rows: [],
  };
  for (let yIndex = 0; yIndex < yMax; yIndex += 1) {
    const row: GridRow = { points: [] };
    for (let xIndex = 0; xIndex < xMax; xIndex += 1) {
      const shot = shots.find(
        (item) => item.point.x === xIndex && item.point.y === yIndex,
      );
      const hasShip = fleet.some((ship) =>
        ship.getCoordinate().isIntersectingWithPoint(new Point(xIndex, yIndex)),
      );
      if (!shot) {
        row.points.push({
          X: xIndex,
          Y: yIndex,
          status: GridPointStatus.NO_SHOOT,
          hasShip,
        });

        // eslint-disable-next-line no-continue
        continue;
      }

      row.points.push({
        X: xIndex,
        Y: yIndex,
        status: shot.warshipIdThatWasHit
          ? GridPointStatus.HIT
          : GridPointStatus.MISS,
        hasShip,
      });
    }
    gridTable.rows.push(row);
  }

  return gridTable;
}
