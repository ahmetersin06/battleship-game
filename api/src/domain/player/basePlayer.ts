import { v4 } from 'uuid';
import Grid from '../grid/grid';
import Point from '../grid/point';
import PointIsOutOfGridRangeError from '../grid/error/pointIsOutOfGridRangeError';
import Shot from './shot';

export default class BasePlayer {
  protected readonly id: string;

  protected readonly enemyGrid: Grid;

  protected readonly shots: Shot[] = [];

  constructor(enemyGrid: Grid) {
    this.enemyGrid = enemyGrid;
    this.id = v4();
  }

  shoot(point: Point): void {
    if (!this.enemyGrid.isPointInGridRange(point)) {
      throw new PointIsOutOfGridRangeError(
        `${point.toString()} is out of grid range!`,
      );
    }

    const warship = this.enemyGrid.findShipByPoint(point);

    this.shots.push({ point, warshipIdThatWasHit: warship?.getId() });
  }

  shootRandomly(): void {
    const randomPoint = this.enemyGrid.getRandomPoint();
    if (this.hasShot(randomPoint)) {
      return this.shootRandomly();
    }

    return this.shoot(randomPoint);
  }

  getShots(): Shot[] {
    return this.shots;
  }

  getHits(): Shot[] {
    return this.shots.filter((shot) => !!shot.warshipIdThatWasHit);
  }

  getEnemyGrid(): Grid {
    return this.enemyGrid;
  }

  getId(): string {
    return this.id;
  }

  protected hasShot(point: Point): boolean {
    return this.shots.some((item) => item.toString() === point.toString());
  }
}
