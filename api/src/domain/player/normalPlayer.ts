import BasePlayer from './basePlayer';
import Player from './player';

export default class NormalPlayer extends BasePlayer implements Player {
  getName(): string {
    return `Player-${this.id}`;
  }
}
