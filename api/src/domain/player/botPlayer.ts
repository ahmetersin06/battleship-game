import BasePlayer from './basePlayer';
import Player from './player';

export default class BotPlayer extends BasePlayer implements Player {
  getName(): string {
    return `Bot-${this.id}`;
  }
}
