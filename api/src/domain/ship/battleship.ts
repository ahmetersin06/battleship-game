import BaseWarship, { WarshipDirection } from './baseWarship';
import Point from '../grid/point';
import Warship from './warship';

export default class Battleship extends BaseWarship implements Warship {
  constructor(startPoint: Point, direction: WarshipDirection) {
    super(startPoint, direction, 5);
  }

  getClassName(): string {
    return 'Battleship';
  }
}
