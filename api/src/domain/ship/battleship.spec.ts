import Battleship from './battleship';
import Point from '../grid/point';
import { WarshipDirection } from './baseWarship';

describe('battleship', () => {
  it('calculates horizontal coordinate correctly', () => {
    const battleship = new Battleship(
      new Point(0, 5),
      WarshipDirection.HORIZONTAL,
    );

    const coordinate = battleship.getCoordinate();
    expect(coordinate.start.toString()).toBe('0,5');
    expect(coordinate.end.toString()).toBe('4,5');
  });

  it('calculates vertical coordinate correctly', () => {
    const battleship = new Battleship(
      new Point(0, 5),
      WarshipDirection.VERTICAL,
    );

    const coordinate = battleship.getCoordinate();
    expect(coordinate.start.toString()).toBe('0,5');
    expect(coordinate.end.toString()).toBe('0,9');
  });
});
