import Point from '../grid/point';
import { WarshipDirection } from './baseWarship';
import Destroyer from './destroyer';

describe('destroyer', () => {
  it('calculates horizontal coordinate correctly', () => {
    const battleship = new Destroyer(
      new Point(0, 5),
      WarshipDirection.HORIZONTAL,
    );

    const coordinate = battleship.getCoordinate();
    expect(coordinate.start.toString()).toBe('0,5');
    expect(coordinate.end.toString()).toBe('3,5');
  });

  it('calculates vertical coordinate correctly', () => {
    const battleship = new Destroyer(
      new Point(0, 5),
      WarshipDirection.VERTICAL,
    );

    const coordinate = battleship.getCoordinate();
    expect(coordinate.start.toString()).toBe('0,5');
    expect(coordinate.end.toString()).toBe('0,8');
  });
});
