import BaseWarship, { WarshipDirection } from './baseWarship';
import Point from '../grid/point';
import Warship from './warship';

export default class Destroyer extends BaseWarship implements Warship {
  constructor(startPoint: Point, direction: WarshipDirection) {
    super(startPoint, direction, 4);
  }

  getClassName(): string {
    return 'Destroyer';
  }
}
