import Point from '../grid/point';
import Warship from './warship';
import Battleship from './battleship';
import { WarshipDirection } from './baseWarship';
import InvalidWarshipType from './error/InvalidWarshipType';
import Destroyer from './destroyer';

export enum WarshipType {
  BATTLESHIP = 'battleship',
  DESTROYER = 'destroyer',
}

export default function createWarship(
  type: WarshipType,
  startPoint: Point,
  direction: WarshipDirection,
): Warship {
  switch (type) {
    case WarshipType.BATTLESHIP:
      return new Battleship(startPoint, direction);
    case WarshipType.DESTROYER:
      return new Destroyer(startPoint, direction);
    default:
      throw new InvalidWarshipType(`${type} is invalid warship type`);
  }
}
