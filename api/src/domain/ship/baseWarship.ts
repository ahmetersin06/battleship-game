import { v4 } from 'uuid';
import Point from '../grid/point';
import Coordinate from '../grid/coordinate';

export enum WarshipDirection {
  HORIZONTAL = 'horizontal',
  VERTICAL = 'vertical',
}

export default abstract class BaseWarship {
  protected coordinate: Coordinate;

  protected readonly size: number;

  protected readonly direction: WarshipDirection;

  protected readonly id: string;

  protected constructor(
    startPoint: Point,
    direction: WarshipDirection,
    size: number,
  ) {
    this.id = v4();
    this.size = size;
    this.direction = direction;
    this.coordinate = BaseWarship.calculateCoordinate(
      startPoint,
      size,
      direction,
    );
  }

  getId(): string {
    return this.id;
  }

  getSize(): number {
    return this.size;
  }

  getCoordinate(): Coordinate {
    return this.coordinate;
  }

  static calculateCoordinate(
    startPoint: Point,
    size: number,
    direction: WarshipDirection,
  ): Coordinate {
    const xLength = direction === WarshipDirection.HORIZONTAL ? size - 1 : 0;
    const yLength = direction === WarshipDirection.VERTICAL ? size - 1 : 0;

    return new Coordinate(
      startPoint,
      new Point(startPoint.x + xLength, startPoint.y + yLength),
    );
  }
}
