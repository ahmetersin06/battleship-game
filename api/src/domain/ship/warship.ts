import Coordinate from '../grid/coordinate';

export default interface Warship {
  getId(): string;
  getClassName(): string;
  getSize(): number;
  getCoordinate(): Coordinate;
}
