import Warship from '../../ship/warship';

export default class ShipIntersectsWithAnotherShipError extends Error {
  constructor(ship: Warship) {
    const startPoint = ship.getCoordinate().start;

    super(
      `${ship.getClassName()}(${startPoint.toString()}) couldn't be added. It intersect with another ship!`,
    );
  }
}
