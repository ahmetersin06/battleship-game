enum GridPointStatus {
  NO_SHOOT = 'NO_SHOOT',
  HIT = 'HIT',
  MISS = 'MISS',
}

export default GridPointStatus;
