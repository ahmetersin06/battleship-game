import Grid from './grid';
import createWarship, { WarshipType } from '../ship/warshipFactory';
import Point from './point';
import { WarshipDirection } from '../ship/baseWarship';
import CoordinateIsOutOfGridRangeError from './error/coordinateIsOutOfGridRangeError';
import ShipIntersectsWithAnotherShipError from './error/shipIntersectsWithAnotherShipError';
import Coordinate from './coordinate';

describe('grid', () => {
  describe('addShip', () => {
    it('adds ship if given grid is available', () => {
      const grid = new Grid(10, 10);

      const ship = createWarship(
        WarshipType.BATTLESHIP,
        new Point(1, 1),
        WarshipDirection.VERTICAL,
      );
      grid.addShip(ship);

      const fleet = grid.getFleet();

      expect(fleet).toHaveLength(1);
      expect(fleet[0]?.getClassName()).toBe('Battleship');
      expect(fleet[0]?.getCoordinate().start.toString()).toBe('1,1');
      expect(fleet[0]?.getCoordinate().end.toString()).toBe('1,5');
    });

    it('throws error if ship coordinate is out of grid range', () => {
      const grid = new Grid(10, 10);

      const ship = createWarship(
        WarshipType.BATTLESHIP,
        new Point(15, 10),
        WarshipDirection.VERTICAL,
      );
      expect(() => grid.addShip(ship)).toThrow(CoordinateIsOutOfGridRangeError);
    });

    it("throws error if ship's coordinate intersects with another ship's coordinate", () => {
      const grid = new Grid(10, 10);

      const ship = createWarship(
        WarshipType.BATTLESHIP,
        new Point(1, 1),
        WarshipDirection.HORIZONTAL,
      );
      grid.addShip(ship);

      expect(() =>
        grid.addShip(
          createWarship(
            WarshipType.BATTLESHIP,
            new Point(2, 1),
            WarshipDirection.VERTICAL,
          ),
        ),
      ).toThrow(ShipIntersectsWithAnotherShipError);
    });
  });

  describe('getRandomPoint', () => {
    it.each([new Grid(10, 10), new Grid(5, 5), new Grid(15, 15)])(
      'gives a random point in grid range',
      (grid: Grid) => {
        const { x: x1, y: y1 } = grid.getRandomPoint();
        const { x: x2, y: y2 } = grid.getRandomPoint();
        const { x: x3, y: y3 } = grid.getRandomPoint();

        expect(x1).toBeLessThan(grid.getWidth());
        expect(x1).toBeGreaterThanOrEqual(0);
        expect(y1).toBeLessThan(grid.getHeight());
        expect(y1).toBeGreaterThanOrEqual(0);

        expect(x2).toBeLessThan(grid.getWidth());
        expect(x2).toBeGreaterThanOrEqual(0);
        expect(y2).toBeLessThan(grid.getHeight());
        expect(y2).toBeGreaterThanOrEqual(0);

        expect(x3).toBeLessThan(grid.getWidth());
        expect(x3).toBeGreaterThanOrEqual(0);
        expect(y3).toBeLessThan(grid.getHeight());
        expect(y3).toBeGreaterThanOrEqual(0);
      },
    );
  });

  describe('isCoordinateIntersectingWithShips', () => {
    it('returns true if coordinate intersecting with one of the ships', () => {
      const grid = new Grid(10, 10);
      grid.addShip(
        createWarship(
          WarshipType.BATTLESHIP,
          new Point(0, 0),
          WarshipDirection.HORIZONTAL,
        ),
      );
      grid.addShip(
        createWarship(
          WarshipType.BATTLESHIP,
          new Point(0, 2),
          WarshipDirection.HORIZONTAL,
        ),
      );
      grid.addShip(
        createWarship(
          WarshipType.DESTROYER,
          new Point(5, 5),
          WarshipDirection.VERTICAL,
        ),
      );

      const coordinate = new Coordinate(new Point(2, 2), new Point(2, 3));
      expect(grid.isCoordinateIntersectingWithShips(coordinate)).toBe(true);
    });

    it('returns false if coordinate not intersecting with any ships', () => {
      const grid = new Grid(10, 10);
      grid.addShip(
        createWarship(
          WarshipType.BATTLESHIP,
          new Point(0, 0),
          WarshipDirection.HORIZONTAL,
        ),
      );
      grid.addShip(
        createWarship(
          WarshipType.BATTLESHIP,
          new Point(0, 2),
          WarshipDirection.HORIZONTAL,
        ),
      );
      grid.addShip(
        createWarship(
          WarshipType.DESTROYER,
          new Point(5, 5),
          WarshipDirection.VERTICAL,
        ),
      );

      const coordinate = new Coordinate(new Point(1, 5), new Point(1, 4));
      expect(grid.isCoordinateIntersectingWithShips(coordinate)).toBe(false);
    });
  });

  describe('findShipByPoint', () => {
    it('return the ship if given point is intersecting with given point', () => {
      const grid = new Grid(10, 10);
      const warship1 = createWarship(
        WarshipType.BATTLESHIP,
        new Point(0, 0),
        WarshipDirection.HORIZONTAL,
      );
      grid.addShip(warship1);
      const warship2 = createWarship(
        WarshipType.BATTLESHIP,
        new Point(0, 2),
        WarshipDirection.HORIZONTAL,
      );
      grid.addShip(warship2);
      const warship3 = createWarship(
        WarshipType.DESTROYER,
        new Point(5, 5),
        WarshipDirection.VERTICAL,
      );
      grid.addShip(warship3);

      const point = new Point(1, 2);
      expect(grid.findShipByPoint(point)).toMatchObject(warship2);
    });

    it('return the ship if given point is not intersecting with given point', () => {
      const grid = new Grid(10, 10);
      grid.addShip(
        createWarship(
          WarshipType.BATTLESHIP,
          new Point(0, 0),
          WarshipDirection.HORIZONTAL,
        ),
      );
      grid.addShip(
        createWarship(
          WarshipType.BATTLESHIP,
          new Point(0, 2),
          WarshipDirection.HORIZONTAL,
        ),
      );
      grid.addShip(
        createWarship(
          WarshipType.DESTROYER,
          new Point(5, 5),
          WarshipDirection.VERTICAL,
        ),
      );

      const point = new Point(1, 3);
      expect(grid.findShipByPoint(point)).toBeUndefined();
    });
  });

  describe('isCoordinateInGridRange', () => {
    it('returns true if coordinate is in grid range', () => {
      const grid = new Grid(10, 10);

      const coordinate = new Coordinate(new Point(2, 2), new Point(2, 3));
      expect(grid.isCoordinateInGridRange(coordinate)).toBe(true);
    });

    it('returns false if coordinate is not in grid range', () => {
      const grid = new Grid(10, 10);

      const coordinate = new Coordinate(new Point(10, 2), new Point(15, 2));
      expect(grid.isCoordinateInGridRange(coordinate)).toBe(false);
    });
  });

  describe('isPointInGridRange', () => {
    it('returns true if point is in grid range', () => {
      const grid = new Grid(10, 10);

      const point = new Point(2, 3);
      expect(grid.isPointInGridRange(point)).toBe(true);
    });

    it('returns false if point is not in grid range', () => {
      const grid = new Grid(10, 10);

      const point = new Point(15, 5);
      expect(grid.isPointInGridRange(point)).toBe(false);
    });
  });

  describe('getFleetSize', () => {
    it('returns total area of the fleet', () => {
      const grid = new Grid(10, 10);
      grid.addShipToRandomPoint(WarshipType.BATTLESHIP);
      grid.addShipToRandomPoint(WarshipType.DESTROYER);
      grid.addShipToRandomPoint(WarshipType.DESTROYER);

      expect(grid.getFleetSize()).toBe(13);
    });
  });
});
