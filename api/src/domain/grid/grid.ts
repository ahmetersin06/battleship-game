import Warship from '../ship/warship';
import createWarship, { WarshipType } from '../ship/warshipFactory';
import Point from './point';
import { WarshipDirection } from '../ship/baseWarship';
import ShipIntersectsWithAnotherShipError from './error/shipIntersectsWithAnotherShipError';
import Coordinate from './coordinate';
import CoordinateIsOutOfGridRangeError from './error/coordinateIsOutOfGridRangeError';

export default class Grid {
  private readonly height: number;

  private readonly width: number;

  private readonly fleet: Warship[] = [];

  constructor(width: number, height: number) {
    this.height = height;
    this.width = width;
  }

  addShip(warship: Warship): void {
    if (!this.isCoordinateInGridRange(warship.getCoordinate())) {
      throw new CoordinateIsOutOfGridRangeError(
        `Ship coordinate(${warship.getCoordinate().start.toString()};${warship
          .getCoordinate()
          .end.toString()}) is out of grid range!`,
      );
    }

    if (this.isCoordinateIntersectingWithShips(warship.getCoordinate())) {
      throw new ShipIntersectsWithAnotherShipError(warship);
    }

    this.fleet.push(warship);
  }

  addShipToRandomPoint(type: WarshipType, marginX = 0, marginY = 0): void {
    const direction = Math.round(Math.random())
      ? WarshipDirection.VERTICAL
      : WarshipDirection.HORIZONTAL;

    const point = this.getRandomPoint(marginX, marginY);
    const ship = createWarship(type, point, direction);
    if (
      this.isCoordinateIntersectingWithShips(ship.getCoordinate()) ||
      !this.isCoordinateInGridRange(ship.getCoordinate())
    ) {
      return this.addShipToRandomPoint(
        type,
        direction === WarshipDirection.HORIZONTAL ? ship.getSize() : 0,
        direction === WarshipDirection.VERTICAL ? ship.getSize() : 0,
      );
    }

    return this.addShip(ship);
  }

  isCoordinateIntersectingWithShips(coordinate: Coordinate): boolean {
    return this.fleet.some((item) => {
      let intersect = false;
      for (let { x } = coordinate.start; x <= coordinate.end.x; x += 1) {
        intersect =
          intersect ||
          item
            .getCoordinate()
            .isIntersectingWithPoint(new Point(x, coordinate.start.y));
      }

      for (let { y } = coordinate.start; y <= coordinate.end.y; y += 1) {
        intersect =
          intersect ||
          item
            .getCoordinate()
            .isIntersectingWithPoint(new Point(coordinate.start.x, y));
      }

      return intersect;
    });
  }

  findShipByPoint(point: Point): Warship | undefined {
    return this.fleet.find((item) =>
      item.getCoordinate().isIntersectingWithPoint(point),
    );
  }

  getFleet(): Warship[] {
    return this.fleet;
  }

  getFleetSize(): number {
    return this.fleet.reduce((total, ship) => total + ship.getSize(), 0);
  }

  getRandomPoint(marginX = 0, marginY = 0): Point {
    const x = Math.floor(Math.random() * this.width - marginX);
    const y = Math.floor(Math.random() * this.height - marginY);

    return new Point(x, y);
  }

  getWidth(): number {
    return this.width;
  }

  getHeight(): number {
    return this.height;
  }

  isCoordinateInGridRange(coordinate: Coordinate): boolean {
    return (
      this.isPointInGridRange(coordinate.start) &&
      this.isPointInGridRange(coordinate.end)
    );
  }

  isPointInGridRange(point: Point): boolean {
    const isXInRange = point.x >= 0 && point.x < this.width;
    const isYInRange = point.y >= 0 && point.y < this.width;

    return isXInRange && isYInRange;
  }
}
