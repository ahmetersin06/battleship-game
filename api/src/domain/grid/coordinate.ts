import Point from './point';

export default class Coordinate {
  readonly start: Point;

  readonly end: Point;

  constructor(start: Point, end: Point) {
    this.start = start;
    this.end = end;
  }

  isIntersectingWithPoint(point: Point): boolean {
    const xIntersect = this.start.x <= point.x && point.x <= this.end.x;
    const yIntersect = this.start.y <= point.y && point.y <= this.end.y;

    return xIntersect && yIntersect;
  }
}
