import { config } from 'dotenv';
import setupGraphQLServer from './transportation/graphql/graphql';
import getGraphqlPort from './config/getGraphqlPort';
import GameManager from './domain/game/gameManager';

config();

async function main(): Promise<void> {
  const graphqlPort = getGraphqlPort();
  const gameManager = new GameManager();

  await setupGraphQLServer(graphqlPort, gameManager);
}

// eslint-disable-next-line no-void
void main();
