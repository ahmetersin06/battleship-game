// eslint-disable-next-line @typescript-eslint/no-var-requires
const config = require('./jest.config');

// noinspection JSConstantReassignment
module.exports = {
  ...config,
  roots: ['test'],
};
