# Battleship API
## Installing Dependencies
```shell
yarn install
```

## Running
### Docker
```shell
docker-compose up api
# Graphql starts running on http://localhost:8080/graphql
```
### Manual
```shell
cp .env.sample .env

yarn start
```

## Running Tests

### Unit tests
```shell
yarn test:unit
```

### E2E tests
```shell
yarn test:e2e
```

## Running Lint
```shell
yarn lint
```
